// Відповіді на теоретичеі питання
/* 
1. Опишіть, як можна створити новий HTML тег на сторінці.
Новий тег можна створити за допомогою document.createElement()
а за допомогою appendChild() - додати до докумену

2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть 
можливі варіанти цього параметра.
Цей параметр визначає позицію елементу(тексту) що додаємо відносно елементу 
в якого ми викликали даний метод.
Варіанти
перші два працюють коли у елементу в якого викликали метод є
батьківський елемент
beforebegin - вставить елемент перед тегом елемента в якого викликали
afterbegin - відразу після тегу
beforeend - перед закриваючим тегом елемента
afterend - після закриваючого тегу елемента

3. Як можна видалити елемент зі сторінки?
Видалити елемент ми можемо за допомогою вбудваного методу 
remove();

Перше звісно нам портібно отримати елемент, можна записати у змінну
а потім видалити, або ж відразу видалити
document.body.getElementsByClassName().remove();
можна отримати елемнт за допомогою інших вбудованих методів 



*/

/* 
Завдання
Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку.
Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - 
DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
*/

let array_1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv", 23, 44, 55, true, [1, 5, 'black', 'blue', true]];
function arrayInIndex(array, element = document.body) {

    let elementOl = document.createElement('ol');

    // Рішення з map... Також додав вкладений масив 
    array.map(item => {
        let list = document.createElement('li');

        if (Array.isArray(item)) {
            list.appendChild(arrayInIndex(item, elementOl));
        } else {
            list.textContent = item;
        }
        elementOl.appendChild(list);
    });
    element.appendChild(elementOl);
    return elementOl;
    // array.forEach(elem => {
    //     let list = document.createElement('li');
    //     list.textContent = elem;
    //     elementOl.appendChild(list);
    // });


}

arrayInIndex(array_1);

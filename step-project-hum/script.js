document.addEventListener('DOMContentLoaded', function () {

    function showFirstArticle(index) {
        currentIndex = index;
        showCurrentItem();
        removeactiveClass();
        itemNawElements[currentIndex].parentElement.classList.add('active');
    }

    function removeactiveClass() {
        itemNawElements.forEach((itemNawElement) => {
            itemNawElement.parentElement.classList.remove('active');
        });
    }
    let servicesItems = document.querySelector('.services-items');
    let servicesItemTitle = document.querySelectorAll('.services-item-title');
    function hideArticleContent() {
        let items = document.querySelectorAll('.item');
        items.forEach(content => {
            content.style.display = 'none';
        });
        // display none для services

    }
    function showFirstTitle(tabIndex) {
        servicesItemTitle.forEach((content, index) => {
            if (index === tabIndex) {
                content.style.display = 'flex';
            } else {
                content.style.display = 'none';
            }
        });
    }
    // 
    servicesItems.onclick = function (event) {
        // Приховали весь зміст
        let content = document.querySelectorAll('.services-item-title');
        for (let elemContent of content) {

            console.dir(elemContent);
            elemContent.style.display = 'none';
        }
        // Якщо є вкладка із встановленим класом active - видаляємо, і при кожному наступній вибраній вкладці - видаляємо у попередньої цей клас
        let tabsActive = document.querySelectorAll('.active');
        for (activeElem of tabsActive) {
            activeElem.classList.remove('active');
        }
        // Додаємо до вибраної вкладки клас - active
        event.target.classList.add('active');
        console.dir(event);

        // створюємо змінну і записуємо в ню  атрибут 
        let dataTab = event.target.getAttribute('data-tab');

        // Шукаємо елемент з класом article та відповідним атрибутом
        let selectContent = document.querySelector(`.services-item-title[data-tab="${dataTab}"]`);
        selectContent.style.display = 'flex';
    }
    hideArticleContent();
    showFirstTitle(0);
    //  

    let sliders = document.querySelector('.slider-naw');
    let itemss = document.querySelectorAll('.item');
    let sliderBtnNext = document.querySelector('.slider-btn-next');
    let sliderBtnPrev = document.querySelector('.slider-btn-prev');
    let currentIndex = 0;
    let itemNawElements = document.querySelectorAll('.item-naw');

    // services
    // Отримуємо всі елементи "services-item" та "services-item-content"

    function showCurrentItem() {
        itemss.forEach((elem) => {
            elem.style.display = 'none';
        });
        itemss[currentIndex].style.display = 'block';
    }

    function slideToNextItem() {
        currentIndex = (currentIndex + 1) % itemss.length;
        showCurrentItem();
    }

    function slideToPrevItem() {
        currentIndex = (currentIndex - 1 + itemss.length) % itemss.length;
        showCurrentItem();
    }

    sliderBtnNext.addEventListener('click', function (event) {
        event.preventDefault();
        removeactiveClass();
        slideToNextItem();
    });

    sliderBtnPrev.addEventListener('click', function (event) {
        event.preventDefault();
        removeactiveClass();
        slideToPrevItem();
    });

    itemNawElements.forEach(function (itemNawElement) {
        itemNawElement.addEventListener('click', function (event) {
            event.preventDefault();
            removeactiveClass();
            event.target.parentElement.classList.add('active');

            let dataTab = event.target.parentElement.getAttribute('data-tab');
            let selectContent = document.querySelector(`.item[data-tab="${dataTab}"]`);
            hideArticleContent();
            selectContent.style.display = 'block';
        });
    });

    hideArticleContent();
    currentIndex = 0;
    showFirstArticle(0);
});

$(document).ready(function () {
    $('.slider-naw.owl-carousel').owlCarousel({
        items: 1, // Number of items to display
        loop: true, // Loop the carousel
        nav: true, // Show navigation buttons
        navText: ['<img src="./img/forms/Forma 1.svg" width="10px" height="15px" alt="">', '<img class="nextBtn" src="./img/forms/Forma 2.svg" width="10px" height="15px" alt="">'], // Custom navigation text
        responsive: {
            0: {
                items: 1 // Number of items to display on smaller screens
            },
            768: {
                items: 3 // Number of items to display on medium screens
            },
            1024: {
                items: 4 // Number of items to display on larger screens
            }
        }
    });
});

// 
document.addEventListener('DOMContentLoaded', function () {
    let workItems = document.querySelectorAll('.work-item');
    let workBoxItems = document.querySelectorAll('.work-box-item');

    function showAllItems() {
        workBoxItems.forEach(function (item) {
            item.style.display = 'block';
        });
    }

    function hideAllItems() {
        workBoxItems.forEach(function (item) {
            item.style.display = 'none';
        });
    }

    function filterItems(event) {
        let dataTab = event.target.getAttribute('data-tab');

        if (dataTab === 'all') {
            showAllItems();
        } else {
            hideAllItems();
            workBoxItems.forEach(function (item) {
                if (item.getAttribute('data-tab') === dataTab) {
                    item.classList.add('active');
                    item.style.display = 'block';

                } else {
                    item.classList.remove('active');
                }
            });
        }
    }

    workItems.forEach(function (item) {
        item.addEventListener('click', filterItems);
    });

    const itemsPerPage = 12;
    let visibleItems = itemsPerPage;
    const workBox = document.querySelector('.work-box');
    const moreButton = document.querySelector('.button-loader-work');

    function showItems(startIndex, endIndex) {
        const items = document.querySelectorAll('.work-box-item');
        items.forEach((item, index) => {
            if (index >= startIndex && index < endIndex) {
                item.style.display = 'block';
            } else {
                item.style.display = 'none';
            }
        });
    }

    moreButton.addEventListener('click', function (event) {
        event.preventDefault();
        visibleItems += itemsPerPage;
        showItems(0, visibleItems);

        if (visibleItems >= document.querySelectorAll('.work-box-item').length) {
            moreButton.style.display = 'none';
        }
    });

    // Показати перші 6 елементів при завантаженні сторінки
    showItems(0, visibleItems);


});


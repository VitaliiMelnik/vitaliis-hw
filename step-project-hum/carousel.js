$(document).ready(function () {
    // Ініціалізуйте Owl Carousel
    $('.slider-naw').owlCarousel({
        items: 4, // Кількість елементів, що показуються в каруселі
        loop: true, // Безкінечна прокрутка
        nav: true, // Показувати кнопки "Назад" та "Вперед"
        dots: false, // Приховати крапки навігації
        mouseDrag: true, // Вимкнути перетягування мишею
        touchDrag: true // Вимкнути перетягування на тач-пристроях
    });

    // Кнопки "Назад" та "Вперед"
    $('.slider-btn-prev').click(function () {
        $('.slider-naw').trigger('prev.owl.carousel');
    });
    $('.slider-btn-next').click(function () {
        $('.slider-naw').trigger('next.owl.carousel');
    });

    // Скролл
    $('.slider-naw').on('mousewheel', '.owl-stage', function (e) {
        if (e.deltaY > 0) {
            $('.slider-naw').trigger('prev.owl.carousel');
        } else {
            $('.slider-naw').trigger('next.owl.carousel');
        }
        e.preventDefault();
    });
});

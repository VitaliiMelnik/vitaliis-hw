// Відповіді на теоретичні питання
/* 
1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
Екранування символів це спосіб відображення символів (можна використати для пошуку в строці символу) за допомогою спеціального символу "\" так як у JS 
є символи які виконують іншу функцію


2.Які засоби оголошення функцій ви знаєте?
-Перший спосіб оголошення функції - декларування
Такий спосіб називається Function Declaration
function func(){}
-другий спосіб - записавши у змінну (function expression)):
let func = function(){}

3. Що таке hoisting, як він працює для змінних та функцій?

Hoisting - це підняття змінних або оголошення функції до їх виклику чи використання, 
у випадку з оголошенням function expression - виклик функції має бути після її оголошення, тоді як 
Function Declaration не має значення в якому порядку викликати, також це стосується і оголошення змінних, їх не 
можна використовувати до оголошення

*/

// Практичне завдання
function createNewUser() {
    let userName = prompt(`Enter your name:`).trim();
    let userSurname = prompt(`Enter your Surname:`).trim();
    let birthday = prompt(`Enter your birthday dd.mm.yyyy:`).trim();

    // Операція з датою
    // let year = birthday.slice
    return newUser = {
        _firstName: userName,
        _lastName: userSurname,
        userBirthday: birthday,
        getLogin() {
            return `Login: ${(this._firstName.slice(0, 1) + this._lastName).toLowerCase()}`;
        },
        getAge() {
            let now = new Date();
            let splitYear = birthday.split('.');
            let date = +splitYear[0].trim();
            let month = +splitYear[1].trim() - 1;
            let year = +splitYear[2].trim();
            let date1 = new Date(year, month, date, 0, 0, 0, 0);
            let result = `Your age: ${Math.floor((now - date1) / (1000 * 60 * 60 * 24 * 365))}`;
            return result;
        },
        getPassword() {
            let result = `Your password: ${newUser._firstName.slice(0, 1).toUpperCase()}${newUser._lastName.toLowerCase()}${newUser.userBirthday.slice(-4)}`;
            return result;
        }
    }

}
createNewUser();

console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());

